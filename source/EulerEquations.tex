\input{Header.tex}

% Configuring the document {{{
% -----------------------------------------------------------------------------
\newcommand{\DocumentTitle}{Finite Volume Method for Euler Equations}
\newcommand{\Author}{Viacheslav Mikerov}

\title{\DocumentTitle}

\author{Viacheslav Mikerov, \\
  \small Department of Informatics \\[-0.8ex]
  \small Technical University of Munich, Bayern, Germany \\
  \small \texttt{ViacheslavMikerov@gmail.com} \\
}

\date{\vspace{-2ex}}
% -----------------------------------------------------------------------------
% Configuring the document }}}

% Configure project {{{
% -----------------------------------------------------------------------------

\begin{document}

\maketitle

\begin{abstract}
% Abstract {{{
% -----------------------------------------------------------------------------
  The paper discusses \textit{Euler equations} and numerical analysis for
  solving them.
  In particular, the application of \textit{Godunov's scheme} is considered,
  and \textit{Roe solver} for approximate solutions of the \textit{Riemann
  problems} in the scheme.
  The main focus is on the fundamentals of solving \textit{hyperbolic
  equations} using knowledge of exact processes proceeding in such systems, and
  Euler equations as an example for such equations.
\end{abstract}
% -----------------------------------------------------------------------------
% Abstract }}}

\section{Introduction}
% Introduction section {{{
% -----------------------------------------------------------------------------
\subsection{Hyperbolic equations}

Considering the problem depending on time as well as one spatial variables, a
homogeneous first-order system of partial differential equations (PDE) in \( x
\) and \( t \) has the form
%
\begin{align} \label{eq:hyperbolicProblem}
  \partial_t \mathbf{w}(x,t) + A \partial_x \mathbf{w}(x,t) = 0.
\end{align}
%
Here
\begin{itemize}
  \item \( \mathbf{w}: \mathbb{R} \times \mathbb{R} \to {\mathbb{R}}^m \) --- vector
    with \( m \) components representing the unknown functions (pressure,
    velocity, etc.);
  \item \( A \) is a \( m \times m \) matrix.
\end{itemize}
%
If \( A \) matrix has \( m \) distinct eigenvalues, then the system
\eqref{eq:hyperbolicProblem} of equations can be decomposed into \( m \)
equations modeling the distinct waves moving in different directions with
different speeds.
It is very crucial fact for the solving methods.
The class of problems of the form \eqref{eq:hyperbolicProblem} with this
property has its name --- \textit{hyperbolic}.

Hyperbolic systems of partial differential equations can be used to model a
wide variety of phenomena that involve wave motion or the advective transport
of substances.
% -----------------------------------------------------------------------------
% Introduction section }}}

\subsection{Euler Equations}
% Euler Equations section {{{
% -----------------------------------------------------------------------------
All of CFD, in one form or another, is based on the fundamental governing
equations of fluid dynamics, each of them represents a corresponding
fundamental principle:
%
\begin{itemize}
  \item \textbf{the continuity equation},
    also known as mass conservation, which reflects the principle that
    \textit{mass is conserved};
  \item \textbf{the momentum equation},
    or conservation of momentum --- \textit{Newton's second law};
  \item \textbf{the energy equation},
    or conservation of energy --- \textit{Energy is conserved}.
\end{itemize}
%
In \textit{Euler Equations} these principles are present in the following forms
%
\begin{align} \label{eq:eulerEquations}
  {\left[ \begin{array}{c}
        \rho \\ \rho u \\ E
  \end{array} \right]}_{t}
  + {\left[ \begin{array}{c}
        \rho u \\
        \rho u^2 + p \\
        (E + p) u
  \end{array} \right]}_{x}
  = 0,
\end{align}
%
where
%
\begin{itemize}
  \item \( \rho \) --- density;
  \item \( u \) --- velocity;
  \item \( E \) --- energy;
  \item \( p \) --- pressure.
\end{itemize}
%
Since there is four unknowns and only three equations, the equation of state for
an ideal polytropic gas,
%
\begin{align}
  E = \frac{p}{\gamma - 1} + \frac{1}{2} \rho u^2,
\end{align}
%
is added to the system, where
%
\begin{itemize}
  \item \( \gamma = \frac{c_p}{c_v} \) --- adiabatic exponent,
  \item \( c_p \) --- specific heat at constant pressure,
  \item \( c_v \) --- specific heat at constant volume.
\end{itemize}

Rewrite the equations~\eqref{eq:eulerEquations} to vector form
%
\begin{align} \label{eq:nonlinearHyperbolicEquation}
  \partial_t \mathbf{w} + \partial_x \mathbf{f}(\mathbf{w}) = 0,
\end{align}
%
where
%
\begin{itemize}
  \item \( \mathbf{f}(\mathbf{w}) \) is a \textit{flux term}
    or just \textit{flux}.
\end{itemize}

In fluid dynamics, the \textit{Euler equations} are a set of equations
governing inviscid flow.
An \textit{inviscid flow} is a flow of an ideal fluid that is assumed to have
no viscosity.
The assumption of an inviscid flow simplifies the solutions of the problem.
The compressible form of these equations is widely used in studies of
high-speed aerodynamics. More about fluid dynamics and Euler equations see
\cite{Anderson}.

The system \eqref{eq:eulerEquations} is a homogeneous first-order system of
partial differential equations. The system could be transformed to the form
\eqref{eq:hyperbolicProblem}, where \( A \) is Jacobian of \(
\mathbf{f}(\mathbf{w}) \):
%
\begin{align} \label{eq:jacobian}
  \begin{array}{*3{>{\displaystyle}c}}
    A &\equiv& J(\mathbf{w}),
    \\[0.1in]
    J(\mathbf{w}) &=& \frac{\partial \mathbf{f}}{\partial \mathbf{w}},
    \\[0.1in]
    {(J(\mathbf{w}))}_{ij} &=& \frac{\partial f_i}{\partial w_j}.
  \end{array}
\end{align}
%
It is shown below, that the Jacobian \eqref{eq:jacobian} has three distinct
eigenvalues
%
\begin{align}
  \lambda_i (\mathbf{w}) < \lambda_j (\mathbf{w}), \  i \neq j
\end{align}
%
with corresponding eigenvectors
%
\begin{align}
  J(\mathbf{w}) \mathbf{r}_i (\mathbf{w}) =
  \lambda_i (\mathbf{w}) \mathbf{r}_i (\mathbf{w}).
\end{align}
%
Therefore, Euler equations forms a hyperbolic problem.
% -----------------------------------------------------------------------------
% Euler Equations section }}}

\section{Finite Volume Method}
% Finite Volume Method section {{{
% -----------------------------------------------------------------------------
\textit{Finite volume methods} are closely related to finite difference
methods, and a finite volume method can often be interpreted directly as a
finite difference approximation to the differential equation.
However, finite volume methods are derived on the basis of the integral form of
the conservation law, a starting point that turns out to have many advantages.
Thus, the elaboration of the numerical solution continues in the integral form.

The cell
%
\begin{align}
  C_i = (x_{\frac{i-1}{2}}, x_{\frac{i+1}{2}})
\end{align}
%
is introduced in the spatial domain of the problem.
The integral form of the conservation law gives
%
\begin{align}
  \frac{\dif}{\dif t} \int_{C_{i}}^{} \mathbf{w}(x,t) \dif x =
  \mathbf{f}(\mathbf{w}(x_{\frac{i-1}{2}},t)) -
  \mathbf{f}(\mathbf{w}(x_{\frac{i+1}{2}},t)).
\end{align}
%
Integrating in time from \( t_n \) to \( t_{n+1} \) yields
%
\begin{align}
  \int_{C_{i}}^{} \mathbf{w}(x,t_{n+1}) \dif x -
  \int_{C_{i}}^{} \mathbf{w}(x,t_{n}) \dif x =
  \int_{t_n}^{t_{n+1}} \mathbf{f}(\mathbf{w}(x_{\frac{i-1}{2}},t)) -
  \int_{t_n}^{t_{n+1}} \mathbf{f}(\mathbf{w}(x_{\frac{i+1}{2}},t)).
\end{align}
%
Introducing the average value over interval at time \( t_n \):
%
\begin{align}
  {\mathbf{W}}_i^n = \frac{1}{\Delta x} \int_{C_i}{} w(x,t_n) \dif x;
\end{align}
%
and the average flux along \( x = x_{\frac{i+1}{2}} \):
%
\begin{align}
  {\mathbf{F}}_{\frac{i+1}{2}}^{n} = \frac{1}{\Delta t}
  \int_{t_n}^{t_{n+1}} \mathbf{f}(\mathbf{w}(x_{\frac{i+1}{2}},t)) \dif t.
\end{align}

As a result, the equation \eqref{eq:nonlinearHyperbolicEquation} could be
rewritten as
%
\begin{align} \label{eq:discreteScheme}
  {\mathbf{W}}_{i}^{n+1} = {\mathbf{W}}_{i}^{n} - \frac{\Delta t}{\Delta x}
  ({\mathbf{F}}_{\frac{i+1}{2}}^{n} - {\mathbf{F}}_{\frac{i-1}{2}}^{n})
\end{align}
%
in the domain \( (x_{\frac{i-1}{2}}, x_{\frac{i+1}{2}}) \times (t_n, t_{n+1})
\).

It is obvious that in this discretization scheme \( {\mathbf{W}}_{i}^{n} \) is the goal of
the numerical solution, that represents an approximated value of the unknown vector
\( \mathbf{w} \) in the domain of the cell \( C_i \) in the time interval \(
(t_n, t_{n+1}) \).
The part of the equation with the average flux difference describes how this
value changes from one time interval to another.
For hyperbolic problems, information propagates as moving waves.
For a system of equations there are several waves propagating at different
speeds and perhaps in different directions.
It makes sense to try to use this knowledge of the structure of the solution to
determine a numerical flux functions.
The information propagates in finite speeds (it is shown below that speeds are
just eigenvalues of the \( A \) matrix), then it fair to state that
%
\begin{align}
  {\mathbf{F}}_{\frac{i+1}{2}} = \mathcal{F}({\mathbf{W}}_{i}, {\mathbf{W}}_{i+1}),
\end{align}
%
where
%
\begin{itemize}
  \item \( \mathcal{F} \) is some numerical flux function.
\end{itemize}
%
In \textit{Godunov's scheme}, \( \mathcal{F} \) is a solution of the Riemann
problem in a particular interface between two averages.
% -----------------------------------------------------------------------------
% Finite Volume Method section }}}

\section{Riemann problem}
% Riemann problem section {{{
% -----------------------------------------------------------------------------
A \textit{Riemann problem} consists of a conservation law together with
piecewise constant data having a single discontinuity.

Before searching the solution of Riemann problem for Euler equations, which are
non-linear hyperbolic equations, it is useful to clarify the problem and state
the terminology of it.
And the best way to achieve this is to solve the problem for simpler cases.

For linear scalar case, the system of equations \eqref{eq:hyperbolicProblem} is
an advection equation,
%
\begin{align}
  w_t + u w_x = 0,
\end{align}
%
and the initial piecewise for the Riemann problem,
%
\begin{align}
  w_0(x) = \left\{
    \begin{array}{cc}
      w_R, & x > 0, \\
      w_L, & x < 0
    \end{array} \right.
\end{align}
%
Evidently, the solution in this case is
%
\begin{align}
  w(x,t) = w_0(x - ut).
\end{align}
%
The matrix \( A \) is \( 1 \times 1 \) value \( u \). The single eigenvalue
and vector are
%
\begin{align}
  \lambda = u, \  r = 1.
\end{align}

Therefore, the solution of the Riemann problem consists of the
discontinuity \( w_R - w_L \) propagating at speed \( \lambda \), along the
direction \( r \).
This method is a key for solving more complicated problems.
Hence, curves along which the partial differential equations becomes an
ordinary differential equation (ODE), are \textit{characteristic curves} or
just \textit{characteristics}.
Once the ODE is found, it can be solved along the characteristic curves and
transformed into a solution for the original PDE.

Now, rescale the linear problem to \( m \) dimensions and using the hyperbolic
problem definition transform it in a form of \( m \) independent equations:
%
\begin{align}
  \partial_t \mathbf{w} + A \partial_x \mathbf{w} = 0,
\end{align}
%
where
%
\begin{itemize}
  \item \( A \) --- \( (m \times m) \) scalar matrix.
\end{itemize}
%
In this case the Riemann problem is stated in the same fashion.
%
\begin{align}
  \mathbf{w}_0(x) = \left\{
    \begin{array}{cc}
      \mathbf{w}_R, & x > 0, \\
      \mathbf{w}_L, & x < 0
    \end{array} \right.
\end{align}
%
is a discontinuity which expands in eigenvectors,
%
\begin{equation} \begin{aligned}
    \mathbf{w}_R &=  R \mathbf{\alpha}, \\
    \mathbf{w}_L &=  R \mathbf{\beta},
\end{aligned} \end{equation}
%
where
\( R \) --- matrix of eigenvectors.
%
\begin{align}
  {k}_i(x) = \left\{
    \begin{array}{cc}
      \alpha_i, & x \leq 0, \\
      \beta_i, & x > 0
    \end{array} \right., \  i = 1, \dots, m.
\end{align}

The solution is discontinuities \( \alpha_i - \beta_i \) moving
along respective eigenvector \( \mathbf{r}_i \) with respective speed of
eigenvalue \( \lambda_i \),
%
\begin{align}
  \mathbf{w}(x,t) = k_i(x - \lambda_i t) \mathbf{r}_i.
\end{align}

In the case of the system of equations the solutions is a superposition of
waves moving along characteristics \( \mathbf{r}_i, i = 1, \dots, m \).

Now, it is time to move back to Riemann problem for Euler equations. For this
the system \eqref{eq:eulerEquations} is transformed to the form
\eqref{eq:hyperbolicProblem} using Jacobian \eqref{eq:jacobian} of flux
function.

The Jacobian is
%
\begin{align}
  J(\mathbf{w}) = \left[
    \begin{array}{ccc}
      0 & 1 & 0 \\
      \frac{1}{2} (\gamma - 3) u^2 &
      (3 - \gamma) u &
      \gamma - 1 \\
      \frac{1}{2} (\gamma - 1) u^3 - u H &
      H - (\gamma - 1) u^2 &
      \gamma u
  \end{array} \right],
\end{align}
%
where
%
\begin{itemize}
  \item \( H = \frac{E + p}{\rho} \) --- total specific enthalpy.
\end{itemize}


Eigenvectors of the Jacobian are
%
\begin{align}
  {\mathbf{r}}_1(\mathbf{w}) = \left[
    \begin{array}{c}
      1 \\ u -c \\ H - u c
  \end{array} \right], \
  {\mathbf{r}}_2(\mathbf{w}) = \left[
    \begin{array}{c}
      1 \\ u \\ \frac{1}{2} u^2
  \end{array} \right], \
  {\mathbf{r}}_3(\mathbf{w}) = \left[
    \begin{array}{c}
      1 \\ u + c \\ H + u c
  \end{array} \right],
\end{align}
%
and respective eigenvalues,
%
\begin{align}
  \lambda_1(\mathbf{w}) = u - c, \
  \lambda_2(\mathbf{w}) = u, \
  \lambda_3(\mathbf{w}) = u + c,
\end{align}
%
where
%
\begin{itemize}
  \item \( c = \sqrt{(\gamma - 1) (H - \frac{1}{2} u^2)} \) --- speed of sound.
\end{itemize}

At this point, it is fair to assume that the solution of Riemann problem is not
just discontinuities propagating in constant speed, because characteristics as
well as speeds know depends on the state of the system. The solution deforms as
it evolves, compression waves or expansion waves can form. To confirm this
assumption the directional derivative of each eigenvalue in the direction of
corresponding eigenvector should be explored.
%
\begin{align}
  \begin{array}{*3{>{\displaystyle}c}}
    \nabla \lambda_1 {\mathbf{r}}_1 &=& \frac{1}{2} (\gamma + 1),
    \\[0.1in]
    \nabla \lambda_2 {\mathbf{r}}_2 &=& 0,
    \\[0.1in]
    \nabla \lambda_3 {\mathbf{r}}_3 &=& \frac{1}{2} (\gamma + 1).
  \end{array}
\end{align}

To interpret this results an \textit{integral curve} of the vector field \(
\mathbf{r}(\mathbf{w}) \) is introduced as a curve \( R \) in the \( \mathbf{w}
\) space with tangent vector \( \mathbf{r} \) at each point; i.e., a curve
whose parametrization \( \mathbf{w}(\mathbf{\xi}) \) satisfies
%
\begin{align}
  \mathbf{w}'(\mathbf{\xi}) = \mathbf{r}(\mathbf{w}(\mathbf{\xi}))
\end{align}
%
at every point.

The characteristic field \( (\lambda, \mathbf{r}) \) is \textit{genuinely nonlinear} if
the directional derivative of \( \lambda \) in the direction of \( \mathbf{r}
\) be everywhere nonzero,
%
\begin{align}
  \nabla \lambda(\mathbf{w}) \mathbf{r}(\mathbf{w}) \neq 0
\end{align}
%
Otherwise, the characteristic field \( (\lambda(\mathbf{w}),
\mathbf{r}(\mathbf{w})) \) is \textit{genuinely linear}
%
\begin{align}
  \nabla \lambda(\mathbf{w}) \mathbf{r}(\mathbf{w}) \equiv 0
\end{align}

\begin{itemize}
  \item The 1st characteristic and 3rd characteristic fields are genuinely
    nonlinear, it means that \( \lambda_1 \) and \( \lambda_2 \) vary along the
    integral curves of these characteristic curves, sharpening into
    \textit{shock wave} or spreading out as \textit{rarefaction waves}.
  \item The 2nd characteristic field is just a wave propagating with the
    constant speed \( \lambda_2 \) along the integral curve without distorting.
    If the initial data is a jump discontinuity, with \( {\mathbf{w}}_L \) and
    \( {\mathbf{w}}_R \) both lying on a single integral curve of this field,
    then the solution will consist of this discontinuity propagating at the
    constant speed associated with this integral curve. This characteristic
    field is called \textit{contact discontinuity}.
\end{itemize}

Riemann invariants are functions of \( \mathbf{w} \) that are constant along
any integral curve of a characteristic field and hence are constant through any
simple wave in this characteristic family. Knowing these functions is very
helpful in constructing solution to the Riemann problem.

\begin{description}
  \item[1st Riemann invariants]: \( s \), \( u + \frac{2 c}{\gamma - 1} \)
  \item[2st Riemann invariants]: \( u \), \( p \)
  \item[3st Riemann invariants]: \( s \), \( u - \frac{2 c}{\gamma - 1} \)
\end{description}

Solutions to a hyperbolic system of \( m \) equations are generally quite
complicated, since at any point there are typically \( m \) waves passing by, moving
at different speeds, and what we observe is some superposition of these waves.
In the nonlinear case the waves are constantly interacting with one another as
well as deforming separately, leading to problems that generally cannot be
solved analytically. It is therefore essential to look for special situations in
which a single wave from one of the characteristic families can be studied in
isolation.

% -----------------------------------------------------------------------------
% Riemann problem section }}}

\section{Approximation of a Riemann problem solution}
% Approximation of the Riemann problem solution section {{{
% -----------------------------------------------------------------------------
The fact that so little information from the Riemann solution is used in
Godunov’s method suggests that one may be able to approximate the Riemann
solution and still obtain reasonable solutions.

One very natural approach to defining an approximate Riemann solution is to
replace the nonlinear problem \eqref{eq:nonlinearHyperbolicEquation} by some
linearized problem defined locally at each cell interface,
%
\begin{align} \label{eq:linearizedNonlinearHyperbolicEquation}
  \partial_t \mathbf{w} + M({\mathbf{W}}_{i}, {\mathbf{W}}_{i+1}) \partial_x \mathbf{f}(\mathbf{w}) = 0,
\end{align}
%
where
%
\begin{itemize}
  \item \( M({\mathbf{W}}_{i}, {\mathbf{W}}_{i+1}) \) is a matrix is chosen to be some approximation to \(
    J(\mathbf{w}) \) valid in a neighborhood of the data \( {\mathbf{W}}_{i} \) and \(
    {\mathbf{W}}_{i+1} \). The matrix \( M({\mathbf{W}}_{i}, {\mathbf{W}}_{i+1}) \) should satisfy the following conditions:
    \begin{enumerate}
      \item Be diagonalizable with real eigenvalues, so that \eqref{eq:linearizedNonlinearHyperbolicEquation} is hyperbolic;
      \item Tend to the original non-linear problem:
        \begin{align}
          M({\mathbf{W}}_{i}, {\mathbf{W}}_{i+1}) \to J(\mathbf{W}) \text{ as }
          {\mathbf{W}}_{i}, {\mathbf{W}}_{i+1} \to \mathbf{W},
        \end{align}
%
        where
%
        \begin{itemize}
          \item \( {\mathbf{W}} \) is an exact solution of the Riemann problem;
        \end{itemize}
%
        so that the method is consistent with the original conservation law.

        \item Conserve the system values:
%
          \begin{align}
            {\mathbf{F}}_{\frac{i+1}{2}} - {\mathbf{F}}_{\frac{i-1}{2}} =
            M({\mathbf{W}}_{i}, {\mathbf{W}}_{i+1}) ({\mathbf{W}}_{i+1} - {\mathbf{W}}_{i}).
          \end{align}

    \end{enumerate}
\end{itemize}

For the Euler equations, \textit{Roe} proposed the following averages:

\begin{itemize}
  \item For the velocity,
%
    \begin{align}
      \overline{u} = \frac{\sqrt{\rho_{i}} u_{i} + \sqrt{\rho_{i+1}} u_{i+1}}
                {\sqrt{\rho_{i}} + \sqrt{\rho_{i+1}}};
    \end{align}
%
  \item For the total specific enthalpy,
%
    \begin{align}
      \overline{H} = \frac{\sqrt{\rho_{i}} H_{i} + \sqrt{\rho_{i+1}} H_{i+1}}
                {\sqrt{\rho_{i}} + \sqrt{\rho_{i+1}}};
    \end{align}
%
  \item For the sound speed,
%
    \begin{align}
      \overline{c} = \sqrt{(\gamma -1)
                 (\overline{H} - \frac{1}{2} \overline{u}^2)}.
    \end{align}
\end{itemize}
%
Using this averages as a current system state for Jacobian. As a result, the
non-linear system  \eqref{eq:nonlinearHyperbolicEquation} becomes a linear
hyperbolic system. Thus, for each interface there is a linear system and
a Riemann problem, which could be solve exactly.

The primary goal for linearization is to find a flux for the discretization scheme
\eqref{eq:discreteScheme}. The eigendecompositin for an approximated Jacobian
gives the equation
%
\begin{align}
  \partial_t \mathbf{w}(x,t) + R D R^{-1} \partial_x \mathbf{w}(x,t) = 0,
\end{align}
%
where
%
\begin{itemize}
  \item \( R \) --- the matrix of right-hand eigenvectors;
  \item \( D \) --- the diagonal matrix of respective eigenvalues.
\end{itemize}
%
Eventually, the equation equivalent to
%
\begin{align}
  \partial_t \mathbf{q}(x,t) + D \partial_x \mathbf{q}(x,t) = 0,
\end{align}
%
where
%
\begin{itemize}
  \item \( \mathbf{q}(x,t) \equiv R^{-1} \mathbf{w}(x,t) \).
\end{itemize}
%
This equation represents \( 3 \) independent advection equations.
Hence, the Riemann problem for a cell interface could be solved exactly by the
following mean:
%
\begin{align}
  {\mathbf{F}}_{\frac{i+1}{2}} = \frac{1}{2} M ({\mathbf{W}}_{i+1} +
  {\mathbf{W}}_{i}) - \frac{1}{2} R |D| R^{-1} ({\mathbf{W}}_{i+1} -
  {\mathbf{W}}_{i}).
\end{align}

The Roe solver has two drawbacks:

\begin{itemize}
  \item it requires \textit{sonic entropy fix}, \cite[sec. 15.3.5]{LeVeque};
  \item it can fail when data is near ``vacuum state'' (\( \rho \approx 0 \)).
\end{itemize}

% -----------------------------------------------------------------------------
% Approximation of the Riemann problem solution section }}}

\section{Implementation}
\label{sec:implementation}
% Implementation {{{
% -----------------------------------------------------------------------------
The implementation is one-dimensional Euler equations simulation using Roe
solver written in C++.
The code is located in a public git \cite{Git} repository, url:
\url{https://bitbucket.org/WscriChy/ 1d-euler-equations/}.
Compilation prerequisites and instructions could be found in the
\textit{``ReadMe.md''} file.

OpenCl \cite{OpenCl} is used for the problem computations. Changes to the
solver as well as boundary conditions and initialization data could be applied
without recompilation of the application by modifying \textit{``Kernel.cl''},
which is an OpenCl file.

The boundary conditions are \textit{solid walls} that the fluid reflects at
this boundaries in a particular way,
%
\begin{align}
  p_{0} &= p_{1}, \nonumber \\
  {\rho}_{0} &= {\rho}_{1}, \\
  u_{0} &= -u_{1} \nonumber
\end{align}
%
for the left boundary. The same approach is applied for the right one.

There are three initial functions: {\code init}, {\code init2}, {\code init3}.
The default one is {\code init}, which is the \textit{dam-break problem}.
This is a special case of the Riemann problem in which \( u_L = u_R = 0 \),
and is called the dam-break problem because it models what happens if a dam separating
two levels of water bursts at time \( t = 0 \).

The initialization function could be changed in the function {\code
computeFlux} in the following lines:

\begin{cpp}
  if (config->init) {
    init(x, config->xSize, currentCell);
    return;
  }
\end{cpp}
% -----------------------------------------------------------------------------
% Implementation }}}

% References {{{
% -----------------------------------------------------------------------------
\begin{thebibliography}{100}
  \bibitem{LeVeque} R.J. LeVeque, \emph{Finite Volume Methods for Hyperbolic
    Problems}, vol. 31, Cambridge University Press, 2002.
  \bibitem{Anderson} John D. Anderson, Jr., \emph{Computational Fluid Dynamics,
    The basics with applications}, 1995.
  \bibitem{Git} \emph{Git manual page}, URL: \url{http://gitmanual.org/}.
  \bibitem{OpenCl} \emph{The open standard for parallel programming of
  heterogeneous systems}, URL: \url{http://www.khronos.org/opencl/}.
\end{thebibliography}
% -----------------------------------------------------------------------------
% References }}}

\end{document}
