\input{PresentationHeader.tex}

\title{Euler Equations}
\author{Viacheslav Mikerov}
\institute{Technical University of Munich}
\date{Hyperbolic PDEs, 2013}
\subject{Informatik}

\begin{document}
\frame{\titlepage}

\begin{frame}
  \frametitle{Equations}
  \begin{align}
    {\left[ \begin{array}{c}
          \rho \\ \rho u \\ E
    \end{array} \right]}_{t}
    + {\left[ \begin{array}{c}
          \rho u \\
          \rho u^2 + p \\
          (E + p) u
    \end{array} \right]}_{x}
    = 0,
  \end{align}
  %
  where
  %
  \begin{itemize}
    \item \( \rho \) --- density;
    \item \( u \) --- velocity;
    \item \( E \) --- energy;
    \item \( p \) --- pressure.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Fundamental Principles Of Fluid Dynamics}
  All of CFD, in one form or another, is based on the fundamental governing
  equations of fluid dynamics, each of them represents a corresponding
  fundamental principle:
  %
  \begin{itemize}
    \item \textbf{the continuity equation}, which reflects the principle that
      \textit{mass is conserved};
    \item \textbf{the momentum equation} --- \textit{Newton's second law};
    \item \textbf{the energy equation} --- \textit{Energy is conserved}.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{The General Transport Theorem}
  Let \( \mathbf{F} \) be a smooth vector field on a region \( \mathcal{R}(t)
  \) whose boundary is \( \mathcal{S}(t) \), let \( \mathbf{W} \) be the
  velocity field of the time-dependent movement of \( \mathcal{S}(t) \). Then
  \begin{align}
    \frac{\dif}{\dif t} \int_{\mathcal{R}(t)}^{} \mathbf{F}(x,t) \dif V =
    \int_{\mathcal{R}(t)}^{} \frac{\partial \mathbf{F}(x,t)}{\partial t} \dif V +
    \int_{\mathcal{S}(t)}^{} \mathbf{F}(x,t) \mathbf{W}(x,t) \cdot \mathbf{n} \dif A
  \end{align}
\end{frame}

\begin{frame}
  \frametitle{The Continuity equation}
  \begin{align}
    \frac{\dif}{\dif t} \int_{\mathcal{R}(t)}^{} \mathbf{F}(x,t) \dif V =
    \int_{\mathcal{R}(t)}^{} \frac{\partial \mathbf{F}(x,t)}{\partial t} \dif V +
    \int_{\mathcal{S}(t)}^{} \mathbf{F}(x,t) \mathbf{W}(x,t) \cdot \mathbf{n} \dif A
  \end{align}
  \begin{align}
    \begin{array}{ccc}
      \text{Mass flow out} && \text{time rate of} \\
      \text{of control volume} &=& \text{decrease of mass} \\
      \text{through surface \( \mathcal{S} \)} &&\text{inside control volume}
    \end{array} \nonumber
  \end{align}
  \begin{align}
    \frac{\partial \rho}{\partial t}
    + \frac{\partial (\rho u)}{\partial x} = 0
  \end{align}
\end{frame}

\begin{frame}
  \frametitle{The Momentum equation}
  Newton's second law
  %
  \begin{align}
    m \mathbf{a} = \mathbf{F}
  \end{align}
  %
  \begin{itemize}
    \item Body forces
      \begin{itemize}
        \item Gravity forces
        \item Electro-magnetic
      \end{itemize}
    \item Surface forces
      \begin{itemize}
        \item Pressure
        \item Viscous
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{The Momentum equation}
  \begin{align}
    m \mathbf{a} = \mathbf{F}
  \end{align}
  \begin{align}
    \frac{\dif}{\dif t} \int_{\mathcal{R}(t)}^{} \mathbf{F}(x,t) \dif V =
    \int_{\mathcal{R}(t)}^{} \frac{\partial \mathbf{F}(x,t)}{\partial t} \dif V +
    \int_{\mathcal{S}(t)}^{} \mathbf{F}(x,t) \mathbf{W}(x,t) \cdot \mathbf{n} \dif A
  \end{align}
  \begin{align}
    \frac{\partial (\rho u)}{\partial t}
    + \frac{\partial (\rho u^2)}{\partial x} =
    - \frac{\partial p}{\partial x}
  \end{align}
\end{frame}

\begin{frame}
  \frametitle{The Energy equation}
  \begin{align}
    \frac{\dif}{\dif t} \int_{\mathcal{R}(t)}^{} \mathbf{F}(x,t) \dif V =
    \int_{\mathcal{R}(t)}^{} \frac{\partial \mathbf{F}(x,t)}{\partial t} \dif V +
    \int_{\mathcal{S}(t)}^{} \mathbf{F}(x,t) \mathbf{W}(x,t) \cdot \mathbf{n} \dif A
  \end{align}
  \begin{align}
    \begin{array}{ccccc}
      \text{Rate of change} && \text{Net flux of} && \text{Rate of work done
    on}\\
    \text{of energy inside} &=& \text{heat into}  &+& \text{element due to} \\
    \text{fluid element} &&\text{element} &&\text{body and surface forces}
  \end{array} \nonumber
\end{align}
\begin{align}
  \frac{\partial E}{\partial t}
  + \frac{\partial (Eu)}{\partial x} =
  - \frac{\partial (p u)}{\partial x}
\end{align}
\end{frame}

\begin{frame}
  \frametitle{Euler Equations}
  \begin{align} \label{eq:eulerEquations}
    {\left[ \begin{array}{c}
          \rho \\ \rho u \\ E
    \end{array} \right]}_{t}
    + {\left[ \begin{array}{c}
          \rho u \\
          \rho u^2 + p \\
          (E + p) u
    \end{array} \right]}_{x}
    = 0,
  \end{align}
  %
  where
  %
  \begin{itemize}
    \item \( \rho \) --- density;
    \item \( u \) --- velocity;
    \item \( E \) --- energy;
    \item \( p \) --- pressure.
  \end{itemize}


  In fluid dynamics, the \textit{Euler equations} are a set of equations
  governing inviscid flow.
\end{frame}

\begin{frame}
  \frametitle{Euler Equations}
  Rewrite the equations~\eqref{eq:eulerEquations} to vector form
  %
  \begin{align} \label{eq:nonlinearHyperbolicEquation}
    \partial_t \mathbf{w} + \partial_x \mathbf{f}(\mathbf{w}) = 0.
  \end{align}
  %
  where
  %
  \begin{itemize}
    \item \( \mathbf{f}(\mathbf{w}) \) is a \textit{flux term}
      or just \textit{flux}.
  \end{itemize}

  \begin{align} \label{eq:hyperbolicProblem}
    \partial_t \mathbf{w}(x,t) + A \partial_x \mathbf{w}(x,t) = 0
  \end{align}

  \begin{align} \label{eq:jacobian}
    \begin{array}{*3{>{\displaystyle}c}}
      A &\equiv& J(\mathbf{w}),
      \\[0.1in]
      J(\mathbf{w}) &=& \frac{\partial \mathbf{f}}{\partial \mathbf{w}},
      \\[0.1in]
      {(J(\mathbf{w}))}_{ij} &=& \frac{\partial f_i}{\partial w_j}.
    \end{array}
  \end{align}
\end{frame}

\begin{frame}
  \frametitle{Finite Volume Method}
  The average value over interval at time \( t_n \):
  %
  \begin{align}
    {\mathbf{W}}_i^n = \frac{1}{\Delta x} \int_{C_i}{} w(x,t_n) \dif x;
  \end{align}
  \begin{align}
    C_i = (x_{\frac{i-1}{2}}, x_{\frac{i+1}{2}})
  \end{align}

  The average flux along \( x = x_{\frac{i+1}{2}} \):
  %
  \begin{align}
    {\mathbf{F}}_{\frac{i+1}{2}}^{n} = \frac{1}{\Delta t}
    \int_{t_n}^{t_{n+1}} \mathbf{f}(\mathbf{w}(x_{\frac{i+1}{2}},t)) \dif t.
  \end{align}

  \begin{align} \label{eq:discreteScheme}
    {\mathbf{W}}_{i}^{n+1} = {\mathbf{W}}_{i}^{n} - \frac{\Delta t}{\Delta x}
    ({\mathbf{F}}_{\frac{i+1}{2}}^{n} - {\mathbf{F}}_{\frac{i-1}{2}}^{n})
  \end{align}
\end{frame}

\begin{frame}
  \frametitle{Godunov's scheme}
  For hyperbolic problems, information propagates as moving waves.
  For a system of equations there are several waves propagating at different
  speeds and perhaps in different directions.
  It makes sense to try to use this knowledge of the structure of the solution to
  determine a numerical flux functions.
  The information propagates in finite speeds (it is shown below that speeds are
  just eigenvalues of the \( A \) matrix), then it fair to state that
  %
  \begin{align}
    {\mathbf{F}}_{\frac{i+1}{2}} = \mathcal{F}({\mathbf{W}}_{i}, {\mathbf{W}}_{i+1}),
  \end{align}

  In \textit{Godunov's scheme}, \( \mathcal{F} \) is a solution of the Riemann
  problem in a particular interface between two averages.
\end{frame}

\begin{frame}
  \frametitle{Characteristics}
  The Jacobian is
  %
  \begin{align}
    J(\mathbf{w}) = \left[
      \begin{array}{ccc}
        0 & 1 & 0 \\
        \frac{1}{2} (\gamma - 3) u^2 &
        (3 - \gamma) u &
        \gamma - 1 \\
        \frac{1}{2} (\gamma - 1) u^3 - u H &
        H - (\gamma - 1) u^2 &
        \gamma u
    \end{array} \right],
  \end{align}
  %
  where
  %
  \begin{itemize}
    \item \( H = \frac{E + p}{\rho} \) --- total specific enthalpy,
    \item \( \gamma = \frac{c_p}{c_v} \) --- adiabatic exponent,
    \item \( c_p \) --- specific heat at constant pressure,
    \item \( c_v \) --- specific heat at constant volume.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Characteristics}
  Eigenvectors of the Jacobian are
  %
  \begin{align}
    {\mathbf{r}}_1(\mathbf{w}) = \left[
      \begin{array}{c}
        1 \\ u -c \\ H - u c
    \end{array} \right], \
    {\mathbf{r}}_2(\mathbf{w}) = \left[
      \begin{array}{c}
        1 \\ u \\ \frac{1}{2} u^2
    \end{array} \right], \
    {\mathbf{r}}_3(\mathbf{w}) = \left[
      \begin{array}{c}
        1 \\ u + c \\ H + u c
    \end{array} \right],
  \end{align}
  %
  and respective eigenvalues,
  %
  \begin{align}
    \lambda_1(\mathbf{w}) = u - c, \
    \lambda_2(\mathbf{w}) = u, \
    \lambda_3(\mathbf{w}) = u + c,
  \end{align}
  %
  where
  %
  \begin{itemize}
    \item \( c = \sqrt{(\gamma - 1) (H - \frac{1}{2} u^2)} \) --- speed of sound.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Characteristics}
  The characteristic field \( (\lambda, \mathbf{r}) \) is \textit{genuinely nonlinear} if
  the directional derivative of \( \lambda \) in the direction of \( \mathbf{r}
  \) be everywhere nonzero,
  %
  \begin{align}
    \nabla \lambda(\mathbf{w}) \mathbf{r}(\mathbf{w}) \neq 0
  \end{align}
  %
  Otherwise, the characteristic field \( (\lambda(\mathbf{w}),
  \mathbf{r}(\mathbf{w})) \) is \textit{genuinely linear}
  %
  \begin{align}
    \nabla \lambda(\mathbf{w}) \mathbf{r}(\mathbf{w}) \equiv 0
  \end{align}
\end{frame}

\begin{frame}
  \frametitle{Characteristics}
  \mytextbullet The 1st characteristic and 3rd characteristic fields are genuinely
  nonlinear, it means that \( \lambda_1 \) and \( \lambda_2 \) vary along the
  integral curves of these characteristic curves, sharpening into
  \textit{shock wave} or spreading out as \textit{rarefaction waves}.

  \mytextbullet The 2nd characteristic field is just a wave propagating with the
  constant speed \( \lambda_2 \) along the integral curve without distorting.
  If the initial data is a jump discontinuity, with \( {\mathbf{w}}_L \) and
  \( {\mathbf{w}}_R \) both lying on a single integral curve of this field,
  then the solution will consist of this discontinuity propagating at the
  constant speed associated with this integral curve. This characteristic
  field is called \textit{contact discontinuity}.
\end{frame}

\begin{frame}
  \frametitle{Solution of non linear hyperbolic problem}
  Solutions to a hyperbolic system of \( m \) equations are generally quite
  complicated, since at any point there are typically \( m \) waves passing by, moving
  at different speeds, and what we observe is some superposition of these waves.
  In the nonlinear case the waves are constantly interacting with one another as
  well as deforming separately, leading to problems that generally cannot be
  solved analytically. It is therefore essential to look for special situations in
  which a single wave from one of the characteristic families can be studied in
  isolation.
\end{frame}

\begin{frame}
  \frametitle{Approximation of a Riemann problem solution}
  \textbf{Motivation}

  The fact that so little information from the Riemann solution is used in
  Godunov’s method suggests that one may be able to approximate the Riemann
  solution and still obtain reasonable solutions.
\end{frame}

\begin{frame}
  \frametitle{Linearization of a Riemann problem solution}
  One very natural approach to defining an approximate Riemann solution is to
  replace the nonlinear problem by some linearized problem defined locally at
  each cell interface,
  %
  \begin{align} \label{eq:linearizedNonlinearHyperbolicEquation}
    \partial_t \mathbf{w} + M({\mathbf{W}}_{i}, {\mathbf{W}}_{i+1}) \partial_x \mathbf{f}(\mathbf{w}) = 0,
  \end{align}
\end{frame}

\begin{frame}
  \frametitle{Linearization of a Riemann problem solution}
  \mytextbullet Be diagonalizable with real eigenvalues, so that is hyperbolic;

  \mytextbullet Tend to the original non-linear problem:
  \begin{align}
    M({\mathbf{W}}_{i}, {\mathbf{W}}_{i+1}) \to J(\mathbf{W}) \text{ as }
    {\mathbf{W}}_{i}, {\mathbf{W}}_{i+1} \to \mathbf{W},
  \end{align}
  %
  so that the method is consistent with the original conservation law.

  \mytextbullet Conserve the system values:
  %
  \begin{align}
    {\mathbf{F}}_{\frac{i+1}{2}} - {\mathbf{F}}_{\frac{i-1}{2}} =
    M({\mathbf{W}}_{i}, {\mathbf{W}}_{i+1}) ({\mathbf{W}}_{i+1} - {\mathbf{W}}_{i}).
  \end{align}
\end{frame}

\begin{frame}
  \frametitle{Linearization of a Riemann problem solution}
  \begin{itemize}
    \item For the velocity,
      %
      \begin{align}
        \overline{u} = \frac{\sqrt{\rho_{i}} u_{i} + \sqrt{\rho_{i+1}} u_{i+1}}
        {\sqrt{\rho_{i}} + \sqrt{\rho_{i+1}}};
      \end{align}
      %
    \item For the total specific enthalpy,
      %
      \begin{align}
        \overline{H} = \frac{\sqrt{\rho_{i}} H_{i} + \sqrt{\rho_{i+1}} H_{i+1}}
        {\sqrt{\rho_{i}} + \sqrt{\rho_{i+1}}};
      \end{align}
      %
    \item For the sound speed,
      %
      \begin{align}
        \overline{c} = \sqrt{(\gamma -1)
        (\overline{H} - \frac{1}{2} \overline{u}^2)}.
      \end{align}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Linearization of a Riemann problem solution}
  The Riemann problem for a cell interface could be solved exactly by the
  following mean:
  %
  \begin{align}
    {\mathbf{F}}_{\frac{i+1}{2}} = \frac{1}{2} M ({\mathbf{W}}_{i+1} +
    {\mathbf{W}}_{i}) - \frac{1}{2} R |D| R^{-1} ({\mathbf{W}}_{i+1} -
    {\mathbf{W}}_{i}).
  \end{align}
\end{frame}

\begin{frame}
  \frametitle{Application}
\end{frame}

\begin{frame}
  \frametitle{Bye}
  Thank you for the attention!
\end{frame}
\end{document}
